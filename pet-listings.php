<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>My Web Page</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
    text-align: center;
}
</style>
</head>
<body><div id="main">
    
    <h1>Pet Listings</h1>
    <?php
    require "database.php";
    $stmt = $mysqli->prepare("SELECT species, COUNT(species) FROM pets group by SPECIES");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
 
    $stmt->execute();
    
    $stmt->bind_result($species, $count);
    
    echo "<table><tr><th>Species</th><th>Count</th></tr>";
    while($stmt->fetch()){
        printf("<tr><td>%s</td> <td>%s</td></tr>",
            htmlspecialchars($species),
            htmlspecialchars($count)
        );
    }
    
    echo "</table>";
 
    $stmt->close();
    
    require "database.php";
    $stmt = $mysqli->prepare("SELECT species, name, filename, weight, description FROM pets");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
 
    $stmt->execute();
    
    $stmt->bind_result($species, $name, $picture, $weight, $description);
    
    $path = sprintf("/home/jkorotzer/public_html/images/%s", $picture);
    
    while($stmt->fetch()){
        echo "<img src = '".$path."' /><br/>";
        echo "<strong>".$name."</strong><br/>";
        echo $species." - ".$weight;
        echo "<br/>".$description."<br/>";
    }
    
    echo "</table>";
 
    $stmt->close();
    ?>
 
</div></body>
</html>