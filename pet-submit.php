<?php
    require "database.php";
    if(isset($_POST['species']) AND isset($_POST['name']) AND isset($_POST['weight']) AND isset($_POST['description']) AND isset($_FILES['picture'])) {
        $species = $_POST['species'];
        $name = $_POST['name'];
        $weight = (float)$_POST['weight'];
        $description = $_POST['description'];
        $filename = $_FILES['picture']['name'];
        $stmt = $mysqli->prepare("insert into pets (species, name, filename, weight, description) values (?, ?, ?, ?, ?)");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
 
        $stmt->bind_param('sssds', $species, $name, $filename, $weight, $description);
 
        $stmt->execute();
 
        $stmt->close();
        $filename = basename($_FILES['picture']['name']);
        
        if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            echo "Invalid filename";
            //exit;
        }
 
        $full_path = sprintf("/home/jkorotzer/public_html/images/%s", $filename);
 
        if( move_uploaded_file($_FILES['picture']['tmp_name'], $full_path) ){
            header("Location: pet-listings.php");
            exit;
        } else{
            echo "Could not upload the file.";
            //header("Location: add-pet.html");
            //exit;
        }
    } else {
        echo "Not all fields have been set.";
        header("Location: add-pet.html");
        exit;
    }
?>